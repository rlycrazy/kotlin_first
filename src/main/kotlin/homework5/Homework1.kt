package homework5



fun main() {
 val user = createUser("12345678912345678912", "1234567891", "1234567891")
}

//write your function and Exception-classes here

class User(val login: String, val password: String)
class WrongLoginException(val msg: String) : Exception(msg)
class WrongPasswordException(val msg: String) : Exception(msg)
fun createUser(login: String, password: String, confirmPassword: String) : User {
    when {
        login.length !in 1..20 -> throw WrongLoginException("Логин должен содержать от 1 до 20 символов")
        password.length < 10 -> throw WrongPasswordException("Пароль должен содержать как минимум 10 символов")
        password != confirmPassword -> throw WrongPasswordException("Пароль и подтверждение пароля должны совпадать")
        else -> return User(login, password)
    }
}
