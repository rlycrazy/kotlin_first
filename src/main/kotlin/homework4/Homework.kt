package homework4

fun main() {
    // first task testing
    val simba = Lion("simba", 2.1, 10)
    simba.eat("meat")
    println(simba.satiety)

    // second task testing
    val lion = Lion("lion", 2.1, 10)
    val tiger = Tiger("tiger", 2.2, 12)
    val wolf = Wolf("wolf", 1.8, 6)
    val animals = arrayOf(lion, tiger, wolf)
    val meals = arrayOf("meat", "bones", "berries", "flesh")
    feedAnimals(meals, animals)
}

abstract class Animal(val name: String, val height: Double, val weight: Int) {
    abstract val food: Array<String>
    var satiety = 0

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}
class Lion(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("meat", "grass", "bones")
}

class Tiger(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("meat", "grass", "bones", "flesh")
}

class Gipo(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("meat", "grass", "fruits")
}

class Wolf(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("meat", "berries", "bones")
}

class Giraffe(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("leaves", "grass")
}

class Elephant(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("leaves", "grass", "fruits")
}

class Chimpanzee(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("leaves", "bugs", "fruits")
}

class Gorilla(name: String, height: Double, weight: Int) : Animal(name, height, weight) {
    override val food = arrayOf("leaves", "nuts", "fruits")
}

fun feedAnimals(meals: Array<String>, animals: Array<Animal>) {
    for (animal in animals) {
        for (meal in meals) {
            animal.eat(meal)
        }
        // add for testing
        println("${animal.name} поел ${animal.satiety} раза")
    }
}
