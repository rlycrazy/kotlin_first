package homework3

class Lion(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("meat", "grass", "bones")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Tiger(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("meat", "grass", "bones", "flesh")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Gipo(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("meat", "grass", "fruits")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Wolf(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("meat", "berries", "bones")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Giraffe(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("leaves", "grass")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Elephant(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("leaves", "grass", "fruits")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Chimpanzee(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("leaves", "bugs", "fruits")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

class Gorilla(name: String, height: Double, weight: Int) {
    var satiety = 0
    val food = arrayOf("leaves", "nuts", "fruits")

    fun eat(meal: String) {
        if (food.contains(meal)) satiety++
    }
}

fun main() {
    val simba = Lion("simba", 2.1, 10)
    simba.eat("meat")
    println(simba.satiety)

    val tiger = Tiger("tiger", 2.2, 12)
    tiger.eat("meat")
    println(tiger.satiety)

    val gipo = Gipo("gipo", 3.0, 22)
    gipo.eat("grass")
    println(gipo.satiety)

    val wolf = Wolf("wolf", 1.8, 6)
    wolf.eat("bones")
    println(wolf.satiety)

    val giraffe = Giraffe("girafee", 8.2, 20)
    giraffe.eat("grass")
    println(giraffe.satiety)

    val elephant = Elephant("elephant", 6.1, 30)
    elephant.eat("grass")
    println(elephant.satiety)

    val chimpanzee = Chimpanzee("makaka", 0.8, 3)
    chimpanzee.eat("leaves")
    println(chimpanzee.satiety)

    val gorilla = Gorilla("kong", 2.4, 14)
    gorilla.eat("leaves")
    println(gorilla.satiety)
}