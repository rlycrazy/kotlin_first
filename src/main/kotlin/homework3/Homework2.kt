package homework3

fun main() {
    val digit = readLine()!!.toInt()
    print(revertInt(digit))
}

fun revertInt(number: Int) : Int {
    var n = number
    var result = 0
    while (n > 0) {
        result = 10 * result + n % 10
        n /= 10
    }
    return result
}
