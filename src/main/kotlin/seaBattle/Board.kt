package seaBattle

class Board() {

    val board = Array(4) {CharArray(4) {EMPTY} }

    fun create() {
        print('\\')
        for (columns in 0 until board[0].size) {
            print("$columns")
        }
        println()
        for (lines in 0 until board.size) {
            print("$lines")
            for (columns in 0 until board[0].size) {
                print("${board[lines][columns]}")
            }
            println()
        }

    }

    fun createForOpp() {
        print('\\')
        for (columns in 0 until board[0].size) {
            print("$columns")
        }
        println()
        for (lines in 0 until board.size) {
            print("$lines")
            for (columns in 0 until board[0].size) {
                if (board[lines][columns] == SHIP_PART) print(EMPTY)
                else print("${board[lines][columns]}")
            }
            println()
        }

    }

    fun placeShip(ship: Ship) : Boolean {
        var success = true

        for (line in 0 until ship.place.size) {
                val x = ship.place[line][0]
                val y = ship.place[line][1]
                if (board[x][y] == EMPTY) board[x][y] = SHIP_PART
                else {
                    print("${x} $y занято, попробуй снова")
                    success = false
                }
            }
        return success
    }

    fun shoot(shotLine: Int, shotColumn: Int): Boolean {
        if(board[shotLine][shotColumn] == SHIP_PART) {
            board[shotLine][shotColumn] = HIT
            println("Попал! Корабль в точке " + shotLine + ":" + shotColumn)
            this.createForOpp()
            return true
        }

        else if(board[shotLine][shotColumn] == EMPTY) {
            board[shotLine][shotColumn] = MISS
            println("Мимо")
            this.createForOpp()
            return false
        }

        else {
            print("Wrong shooting taget exeption")
            return false
        }
    }

    companion object {
        const val SHIP_PART = '*'
        const val HIT = 'X'
        const val MISS = '-'
        const val EMPTY = '.'
    }
}