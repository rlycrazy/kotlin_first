package seaBattle

class Player(val name: String) {
    var availableShips: Int = 0
    val board = Board()

    fun newShip(n: Int): Ship {
        val deckCount = n

        val shipCoordinates: Array<IntArray> = Array(deckCount) {IntArray(2)}

        for (n in 0 until deckCount) {
            print("Введите координату пабулы номер ${n+1} в формате XY (строка-столбец): ")
            var coordinates = readln().toInt()
            if (coordinates / 10 !in 0..3 || coordinates % 10 !in 0..3) {
                println("Out of field exeption, try again")
                coordinates = readln().toInt()
            }
            shipCoordinates[n][0] = coordinates / 10
            shipCoordinates[n][1] = coordinates % 10
        }

        val ship = Ship(deckCount, shipCoordinates)
        return if (board.placeShip(ship)) {
            availableShips += ship.deck
            return ship
        } else this.newShip(n)
    }

    fun fillBoard() {
        println("\n${this.name}, добавь корабли:")
        this.board.create()
        println("По правилам нужно добавить 2 однопалубных корабля и 1 двухпалубный корабль")
        println("Сначала добавим двухпалубный корабль")
        this.newShip(2)
        println("Теперь добавим 2 однопалубных коробля")
        val shipCount = 2
        for (ship in 0 until shipCount) {
            print("Добавим корабль №${ship+1}. ")
            this.newShip(1)
        }

        println("Вот такая доска получилась:")
        board.create()
    }

    fun step(opponent: Player) {
        println("\nХодит ${this.name} против ${opponent.name}. Поле ${opponent.name} ниже:")
        opponent.board.createForOpp()

        var step = true

        while (step) {
            print("${this.name}, введите координату для выстрела в формате XY (строка-столбец): ")
            val shotCoordinates = readln().toInt()

            val shotLine = shotCoordinates / 10
            val shotColumn = shotCoordinates % 10

            if (shotLine > 3 || shotColumn % 10 > 3) {
                println("Out of field exeption, try again")
            }
            else {
                step = opponent.board.shoot(shotLine, shotColumn)
                if (step) {
                    --opponent.availableShips
                    println("\n${this.name} попал по кораблю!")
                }
                if (opponent.availableShips == 0) step = false
            }
        }
    }
}
