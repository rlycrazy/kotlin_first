package seaBattle

class Game {
    fun startGame() {
        print("Введите имя первого игрока: ")
        val player1 = Player(readln())

        print("Введите имя второго игрока: ")
        val player2 = Player(readln())

        player1.fillBoard()
        player2.fillBoard()

        var game = true

        while (game) {
            player1.step(player2)
            player2.step(player1)
            if(player1.availableShips == 0 || player2.availableShips == 0) game = false
        }

        if (player1.availableShips > 0 && player2.availableShips == 0) {
            println("\nВыиграл игрок 1 по имени ${player1.name}")
        }
        else if (player1.availableShips == 0 && player2.availableShips > 0) {
            println("\nВыиграл игрок 2 по имени ${player2.name}")
        }
    }
}