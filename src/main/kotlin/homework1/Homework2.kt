package homework1

fun main() {
    val limonade : Int = 18500
    val pinaColada : Short = 200
    val whiskey : Byte = 50
    val fresh : Long = 3000000000
    val cola : Float = 0.5F
    val ale : Double = 0.666666667
    val author = "Что-то авторское!"

    println("Заказ - ‘$limonade мл лимонада’ готов!")
    println("Заказ - ‘$pinaColada мл пина колады’ готов!")
    println("Заказ - ‘$whiskey мл виски’ готов!")
    println("Заказ - ‘$fresh капель фреша’ готов!")
    println("Заказ - ‘$cola литра колы’ готов!")
    println("Заказ - ‘$ale литра эля’ готов!")
    println("Заказ - ‘$author’ готов!")
}