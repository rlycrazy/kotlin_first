package homework6

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    //call your functions here
    println(countVegetables(vegetableSet, userCart))
    println(fullPrice(userCart, discountSet, discountValue, prices))
}

//write your functions here
fun countVegetables(vegetables: Set<String>, userCart: Map<String, Int>) : Int {
    var count = 0
    for (i in vegetables) {
        if (userCart.contains(i)) count += userCart.getValue(i)
    }
    return count
}

fun fullPrice(userCart: Map<String, Int>, discountSet: Set<String>, discount: Double, prices: Map<String, Double>) : Double {
    var fullPrice = 0.0
    for (i in userCart.keys) {
        if (prices.containsKey(i)) {
            fullPrice += if (discountSet.contains(i)) (prices.getValue(i) * userCart.getValue(i) * (1 - discount))
            else (prices.getValue(i) * userCart.getValue(i))
        }
    }
    return fullPrice
}
