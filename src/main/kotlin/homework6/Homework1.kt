package homework6

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    //call your function here
    println(listToSet(sourceList))
}

//write your code here
fun listToSet(inputList: List<Int>): Set<Int> = inputList.toSet()