package homework2

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var capitanJo = 0
    var crew = 0
    for (i in myArray) {
        if (i % 2 == 0) capitanJo++
        else crew++
    }
    println("У капитана Джо $capitanJo монет, а у команды $crew")
}
