package homework2

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var excellent = 0.0
    var good = 0.0
    var ok = 0.0
    var bad = 0.0

    for (i in marks) {
        when(i) {
            5 -> excellent++
            4 -> good++
            3 -> ok++
            2 -> bad++
        }
    }

    println("""Отличников - ${ (excellent / marks.count()) * 100 }%
        |Хорошистов - ${ (good / marks.count()) * 100}%
        |Троечников - ${ (ok / marks.count()) * 100}%
        |Двоечников - ${ (bad / marks.count()) * 100}%
    """.trimMargin())
}